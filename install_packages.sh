#!/bin/bash

## Installation des packages et configuration de certains d'entre eux

## Partie I : Configuration
echo "Partie I : Configuration"

# On rend apt sans aucune interaction
export DEBIAN_FRONTEND=noninteractive

# Pour eviter les problemes de locale
LANG=

# On commence par mettre a jour ce qui est installe
apt update -y
apt upgrade -y

# On determine la version du noyau
export KERNEL_VERSION=`cd /boot && ls -1 vmlinuz-* | tail -1 | sed 's@vmlinuz-@@'`

## Partie II : Installation
echo "Partie II : Installation"

apt install -y \
    xubuntu-desktop \
    ocaml \
    inkscape \
    gimp \
    apache2 \
    wget \
    htop \
    language-pack-fr \
    vim \
    emacs \
    python3-pyqt5 \
    zeal \
    libreoffice \
    libreoffice-l10n-fr \
    php-cli \
    sqlite3 \
    git \
    gcc-10 \
    build-essential \
    gdb \
    valgrind \
    gfortran \
    gprolog \
    python3-pip \
    graphviz \
    gnuplot \
    flex \
    ml-yacc \
    ragel \
    menhir \
    python-is-python3 \
    python-dev-is-python3 \
    firefox \
    subversion \
    opam \
    pkg-config \
    zlib1g-dev \
    libffi-dev \
    libgmp-dev \
    libzmq5-dev \
    evince \
    zerofree \
    manpages \
    manpages-dev \
    manpages-fr \
    manpages-fr-dev \
    glibc-doc \
    man-db \
    bash-completion \
    console-data \
    plocate \
    rdiff-backup \
    nfs-common \
    autofs \
    chrony \
    xserver-xorg-input-kbd \
    grub-efi-amd64-signed \
    casper \
    xorriso \
    mtools \
    linux-modules-extra-${KERNEL_VERSION} \
    vlc \
    blender \
    wireshark \
    thonny \
    rxvt-unicode \
    texlive-full

## Logiciels non gérés par apt directement:
# Récupérer le paquet pour Filius
wget https://www.lernsoftware-filius.de/downloads/Setup/filius_latest_all.deb
apt install ./filius_latest_all.deb

# Nettoyer
apt autoclean

# Supprimer tous les paquets téléchargés
rm -rf /var/cache/apt/archive

# PyCharm
snap install pycharm-community --classic

# VScodium
snap install codium --classic

# DB Browser
snap install sqlitebrowser

# Modules python
python3 -m pip install -r packages_python.txt
pip freeze -r packages_python.txt > /etc/skel/packages_python.txt

# Locale
localectl set-locale fr_FR.UTF-8
localectl set-x11-keymap fr

## Configuration dans le squelette
# VSCodium
mkdir -p /etc/skel/.config/VSCodium
mkdir -p /etc/skel/.vscode-oss/extensions

# emacs 
mkdir -p /etc/skel/.emacs.d && cd /etc/skel/.emacs.d && git clone https://github.com/ocaml/tuareg && cd tuareg && make
echo '(load "~/.emacs.d/tuareg/tuareg-site-file")' >> /etc/skel/.emacs

# Nettoyage
rm -rf /var/lib/snapd/cache