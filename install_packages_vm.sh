#!/bin/bash

## Installation des packages et configuration de certains d'entre eux

## Partie I : Configuration
echo "Partie I : Configuration"

# On rend apt sans aucune interaction
#export DEBIAN_FRONTEND=noninteractive

# Pour eviter les problemes de locale
LANG=

# On commence par mettre a jour ce qui est installe
apt update -y
apt upgrade -y

# On determine la version du noyau
export KERNEL_VERSION=`cd /boot && ls -1 vmlinuz-* | tail -1 | sed 's@vmlinuz-@@'`

# On remet a jour avec le nouveau repository
apt update -y

## Partie II : Installation
echo "Partie II : Installation"

apt install -y linux-modules-extra-${KERNEL_VERSION} \

# Nettoyer
apt autoclean

# Supprimer tous les paquets téléchargés
rm -rf /var/cache/apt/archive

## Logiciels non gérés par apt directement:
# Récupérer le paquet pour Filius
wget https://www.lernsoftware-filius.de/downloads/Setup/filius_latest_all.deb
apt install ./filius_latest_all.deb

# PyCharm
snap install pycharm-community --classic

# DB Browser
snap install sqlitebrowser

# Modules python
python3 -m pip install -r packages_python.txt
pip freeze -r packages_python.txt > /etc/skel/packages_python.txt

# Locale
localectl set-locale fr_FR.UTF-8
localectl set-x11-keymap fr

## Configuration dans le squelette
# VSCodium
mkdir -p /etc/skel/.config/VSCodium
mkdir -p /etc/skel/.vscode-oss/extensions

# emacs 
mkdir -p /etc/skel/.emacs.d && cd /etc/skel/.emacs.d && git clone https://github.com/ocaml/tuareg && cd tuareg && make
echo '(load "~/.emacs.d/tuareg/tuareg-site-file")' >> /etc/skel/.emacs

# Nettoyage
rm -rf /var/lib/snapd/cache
