#!/bin/bash

## Script principal pour générer la VM

# Le repertoire depuis lequel il est lance doit avoir suffisamment d'espace libre - 45 Go environ
# Le systeme depuis lequel on fait les manipulations doit avoir d'installe un certain nombre d'outils:
# wget : pour telecharger un fichier depuis la ligne de commande
# gawk : un utilitaire pratique pour traiter les chaines de caracteres en ligne de commande
# rsync : utilitaire pour synchroniser des fichiers
# qemu : emulateur tres repandu pour demarrer une vm
# cloud-init : necessaire pour installer les paquets sur la vm
# cloud-images-utils : afin d'avoir la commande cloud-localds pour faire une image a partir d'un yaml
# virtualbox : afin de pouvoir convertir en .vdi

## PARTIE I : CONFIGURATION
echo "Partie I : CONFIGURATION"

# Configuration des répertoires
export RACINE=`pwd`
export WORKING=${RACINE}/working
export RELEASE=${RACINE}/vm

# Choisir la version de xubuntu voulue : focal = 20.04 LTS, jammy = 22.04 LTS, lunar = 23.04
export VERSION=lunar

# On recupere la memoire RAM libre  de l'ordinateur en octets
RAM_FREE=$( free | grep "^Me" | gawk '{print $4}' )

# On prend pour la machine virtuelle les 2/3 de la RAM libre, ce qui devrait laisser assez de RAM pour le reste
# Et on le convertit en ko
export RAM_VM=$(( ${RAM_FREE} * 2 / 3000 ))

# On recupere le nombre de coeur du cpu hote:
NUM_CORES=$( cat /proc/cpuinfo | grep "cpu cores" | uniq | gawk '{print $4}' )

# On reserve pour la vm la moitie des cores:
export CORES_VM=$(( ${NUM_CORES} / 2 ))

## Partie II : Fichiers auxiliaires
echo "Partie II : Fichiers auxiliaires"

# Création des répertoires
mkdir -p ${WORKING}
mkdir -p ${RELEASE}

# Creation du faux CD d'installation pour cloud-init en local:
cloud-localds ${WORKING}/cloud.img ${RACINE}/cloud-init.yaml

# Récupération de l'image d'ubuntu de base
cd ${WORKING}
wget https://cloud-images.ubuntu.com/${VERSION}/current/${VERSION}-server-cloudimg-amd64.img
cd ${RACINE}

# On agrandit l'image de 20 Go pour qu'elle puisse contenir tout ce qu'on souhaite installer
qemu-img resize ${WORKING}/${VERSION}-server-cloudimg-amd64.img +20G

## Partie III : Installation des logiciels sur la future VM
echo "Partie III : Installation des logiciels sur la future VM"

# On lance la VM avec le disque de cloud-init local:
qemu-system-x86_64 \
    -m ${RAM_VM} \
    -smp ${CORES_VM} \
    -net nic \
    -net user \
    -hda ${WORKING}/${VERSION}-server-cloudimg-amd64.img \
    -hdb ${WORKING}/cloud.img \
    -nographic

## Partie IV : Création du fichier .vdi final pour la vm
echo "Partie IV : Création du fichier .vdi final pour la vm"

# On converti en image montable
qemu-img convert ${WORKING}/${VERSION}-server-cloudimg-amd64.img ${WORKING}/${VERSION}-server-cloudimg-amd64.raw

# On supprime l'ancienne image .img pour faire de la place
rm -f ${WORKING}/${VERSION}-server-cloudimg-amd64.img

# On convertit en vdi
VBoxManage convertdd ${WORKING}/${VERSION}-server-cloudimg-amd64.raw ${RELEASE}/${VERSION}-server-cloudimg-amd64.vdi --format VDI

# On supprime le fichier temporaire raw
rm -f ${WORKING}/${VERSION}-server-cloudimg-amd64.raw

echo "Fini"
