#!/bin/bash

cd
opam init -a
opam install -y jupyter utop ocaml-lsp-server
eval \$(opam env) && ocaml-jupyter-opam-genspec
echo 'eval \$(opam env)' >> ~/.bashrc
echo 'export PATH=~/.local/bin:~/texlive/bin/x86_64-linux:$PATH' >> ~/.bashrc
jupyter kernelspec install --user --name ocaml-jupyter ~/.opam/default/share/jupyter
codium --install-extension ms-ceintl.vscode-language-pack-fr
codium --install-extension ocamllabs.ocaml-platform
codium --install-extension ms-python.python
wget https://github.com/microsoft/vscode-cpptools/releases/download/v1.14.3/cpptools-linux.vsix
codium --install-extension cpptools-linux.vsix
